from statsd import StatsClient
import yaml
config = yaml.load(open('service/app/config.yml'))

statsd = StatsClient(config["statsd"]["host"], config["statsd"]["port"], config["statsd"]["prefix"]+'.'+config["host"]["name"])
