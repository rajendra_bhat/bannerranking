__author__ = 'rajendra'

from flask import request,g
from time import time
from flask import Flask, jsonify
from flask_cors  import CORS
import logging
from logging import handlers
import logstash
import json






service_logger = logging.getLogger('python-logstash-logger')
service_logger.setLevel(logging.INFO)
service_logger.addHandler(logstash.TCPLogstashHandler('localhost', 5000)) 

def get_logger(    
        LOG_FORMAT     = '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        LOG_NAME       = '',
        LOG_FILE_INFO  = 'Test_service.log',
        LOG_FILE_ERROR = 'Test_service.err'):

    log           = logging.getLogger(LOG_NAME)
    log_formatter = logging.Formatter(LOG_FORMAT)

    #comment this to suppress console output
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(log_formatter)
    log.addHandler(stream_handler)

    file_handler_info = logging.FileHandler(LOG_FILE_INFO, mode='a')
    file_handler_info.setFormatter(log_formatter)
    file_handler_info.setLevel(logging.DEBUG)
    log.addHandler(file_handler_info)

    file_handler_error = logging.FileHandler(LOG_FILE_ERROR, mode='a')
    file_handler_error.setFormatter(log_formatter)
    file_handler_error.setLevel(logging.ERROR)
    log.addHandler(file_handler_error)

    log.setLevel(logging.INFO)

    return log

logger = get_logger()

app = Flask(__name__)
CORS(app)

@app.before_request
def before_request():
    g.start = time()

@app.teardown_request
def teardown_request(exception=None):
    diff = time() - g.start
    logger.info(str(request.url) + "|" + str(diff) + " seconds")
    try:
        service_logger.info('Request completed', extra= {'request' : request.get_json(silent = True), 'url' : request.url, 'time_taken' : diff, 'response' : g.response})
        # print g.response
    except AttributeError:
        pass
@app.route('/insights/health/check')
def health_check():
    return ("Success", 200)

import common.endpoints

