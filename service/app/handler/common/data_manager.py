'''
Main entry class for all the routes
'''
import json

from response import *
from handler import logger
import sys


def getRank(data):
    try:
        result = "working fine"
        http_response = CustomResponse(10001,"The call is successful", result)
        return http_response.get_success_message()
    except:
        logger.exception("The input request was not correct - datamanager -")
        logger.error(sys.exc_info())
        http_response = CustomResponse(15001,"The requested call failed - Check your input","")
        return http_response.get_failed_message()

