from aiohttp import web

async def poll(request):
    return web.json_response(text="OK", status=200)
