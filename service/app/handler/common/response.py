'''
This will create the response for client
'''
from bson.json_util import dumps
from flask import Response, g


class CustomResponse():

    def __init__(self, status_code, status_msg, result):
        self.status_code = status_code
        self.status_msg = status_msg
        self.result = result
        g.response = {'status' : status_code, 'message' : status_msg, 'data' : result}

    def get_success_message(self):
        response = {}
        response['status'] = {}
        response['status']['statusType'] = "SUCCESS"
        response["status"]["statusMessage"] = self.status_msg
        response["status"]["statusCode"] = self.status_code

        response["data"] = self.result
        return Response(dumps(response),mimetype='application/json')

    def get_failed_message(self):
        response = {}
        response['status'] = {}
        response["status"]["statusType"] = "FAILURE"
        response["status"]["statusMessage"] = self.status_msg
        response["status"]["statusCode"] = self.status_code

        response["data"] = self.result
        return Response(dumps(response),mimetype='application/json')

