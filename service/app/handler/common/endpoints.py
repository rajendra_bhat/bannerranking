from handler import app,logger
from flask import request, jsonify , send_from_directory
import json
import data_manager
import sys
from response import *

@app.route('/api/test',methods = ['GET'])
def testing():
    print "request received" 
    return "test working"

@app.route('/api/checkSegmentName', methods=['POST'])
def getRank():
    '''
    '''
    try:
        request_data = json.loads(request.data)
    except:
        logger.exception("The input request was not correct - JSON LOADING -")
        http_response = CustomResponse(19001, "The input request is not correct" , "")
        return http_response.get_failed_message()

    return data_manager.getRank(request_data)



