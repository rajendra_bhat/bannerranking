import pickle
import numpy as np
import logging

logger = logging.getLogger('my_logger')


class SizingModel:

    def __init__(self):
        self.shirt_model = pickle.load(open('service/app/resources/gb_model_3_5.p', 'rb'))
        self.tshirt_model = pickle.load(open('service/app/resources/tshirts_model.p', 'rb'))
        self.mapping = {
            'tshirts': self.tshirt_model,
            'shirts': self.shirt_model
        }

    def get_model(self, article_type):
        return self.mapping[article_type]

    def get_sku_id(self, article_type, user_vector, style_lookup_arr, return_percentage):
        """"
        Parameters
        ----------
        user_vector : uidx of the user,
        article_type: to get the correct model
        return_percentage
        style_lookup_arr: array of lookup vectors

        Returns
        -------
        predicted_sku_id : returns the predicted sku id for the given user and style id


        Notes:
        The method returns None if uidx is not present in single persona and if style id is not included in style lookup.
        The style lookup will be updated everyday so it includes the latest styles

        """
        # Getting the brand vector
        brand_vector = [0.0] * 1
        if return_percentage:
            brand_vector[0] = return_percentage

        v_list = []
        predicted_sku_list = []
        for lookup in style_lookup_arr:
            for sku_id in lookup:
                if sku_id not in ['brand', 'article_type']:
                    v = user_vector + eval(lookup[sku_id])[0] + brand_vector
                    v_list.append(v)
                    predicted_sku_list.append(sku_id)

        if len(v_list) == 0:
            return None

        pro = self.get_model(article_type.lower()).predict_proba(v_list)
        predicted_pro = [x[1] for x in pro]
        i = np.argmax(predicted_pro)
        predicted_sku = predicted_sku_list[i]
        # logger.debug("User Vector:{}\nStyleId:{}\nReco:{}\n".format(user_vector, style_lookup, predicted_sku))
        return predicted_sku, 100

