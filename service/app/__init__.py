import argparse
import asyncio
import logging
import traceback
from logging import config
import aioredis
import yaml

from service.app.handler.size_handler import SizeHandler
from service.app.model.sizing_model import SizingModel
from service.app.settings import dictConfig


async def get_redis_conn(host):
    return await aioredis.create_redis((host, 6379), encoding='UTF-8')

try:
    config = yaml.load(open('service/app/config.yml'))
    logging.config.dictConfig(dictConfig)
    logger = logging.getLogger('my_logger')
    redis_host = config['redis']['host']
    parser = argparse.ArgumentParser(description="Sizing Service with Port")
    parser.add_argument('port', type=int)
    args = parser.parse_args()
    port = args.port
    logging.getLogger('asyncio_redis').setLevel(logging.WARNING)
    logger.debug("Initializing Model")
    loop = asyncio.get_event_loop()
    redis_conn = loop.run_until_complete(asyncio.ensure_future(get_redis_conn(redis_host)))
    model = SizingModel()
    size_handler = SizeHandler(redis_conn, model, config['model_config'])
    logging.getLogger('my_logger').debug("Completed Initialization")
except Exception as e:
    traceback.print_exc()
    raise
