dictConfig = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s Module:%(module)s --- %(message)s'
        },
        'simple': {
            'format': '%(levelname)s - %(message)s'
        }
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'formatter': 'verbose',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'service/logs/debug.log',
            'maxBytes': 100000000,
            'backupCount': 4
        },
        'access': {
            'level': 'INFO',
            'formatter': 'simple',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'service/logs/access_log.log',
            'maxBytes': 100000000,
            'backupCount': 4
        },
        'error': {
            'level': 'ERROR',
            'formatter': 'verbose',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'service/logs/error.log',
            'maxBytes': 100000000,
            'backupCount': 4
        }
    },
    'loggers': {
        'my_logger': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': False
        },
        'aiohttp.access': {
            'handlers': ['access'],
            'level': 'INFO',
            'propagate': False
        },
        'aiohttp.server': {
            'handlers': ['error'],
            'level': 'ERROR',
            'propagate': False
        },
        'error_logger': {
            'handlers': ['error'],
            'level': 'ERROR',
            'propagate': False
        }
    }
}
