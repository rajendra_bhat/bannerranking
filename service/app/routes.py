from service.app.handler.health_check import poll
from service.app import size_handler


def setup_routes(app):
    app.router.add_post('/api/v1/recommend', size_handler.get_recommended_size)
    app.router.add_get('/', poll)
