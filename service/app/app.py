'''
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        DO NOT CHANGE ANYTHING HERE
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        This file initialises and run the Server.
        It also initialises the logging file named '.log'
        Use logging.getLogger('.log') to access the same
'''

from handler import app

'''
import os
app_config = os.environ.get('APPLICATION_CONFIG')

PORT_NO = os.environ.get('PORT_NO')
if PORT_NO is None:
        if app_config is not None and "dev" in app_config :
                PORT_NO = 5000
        else:
                raise ValueError("Please specify correct PORT_NO")

'''

if __name__ == '__main__':
    app.run(debug=True,port = 8080,host='0.0.0.0',threaded=True)

#init_logging()
#app.run(debug=True,port=int(PORT_NO),host='0.0.0.0',threaded=True)
