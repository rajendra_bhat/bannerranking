cd /myntra/$APP/service
virtualenv virtualenv_testservice
source virtualenv_sizingservice/bin/activate
mkdir -p /etc/supervisor.d/
cp ops/setup/supervisord.conf /etc/supervisord.conf
cp ops/setup/sizing_service_docker.ini /etc/supervisor.d/sizing_service_docker.ini
echo "Starting supervisord"
supervisord