#!/usr/bin/env bash
cd $WORKSPACE/service
mkdir -p logs
chmod -R 755 logs
./gradlew clean rpm
cp ops/profiles/preprod/config.yml app/
virtualenv -p /usr/local/bin/python2.7 virtualenv_BannerRanking_service
source virtualenv_BannerRanking_service/bin/activate
pip2.7 install -r service_requirements.txt
deactivate
